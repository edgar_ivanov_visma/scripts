 <#
.Synopsis
   	Force Install Updates on Remote Computer (it is possible to incule optional updates or exclude individual KB exceptions, restart if required)
.DESCRIPTION
   	Force Install Updates on Remote Computer using a scheduled task
    Script generates available updates list
    Excludes individual KB if given  (-excludelist 3140250, 3140234 -includeoptionalupdates -rebootifrequired)
    Sets sheduled job on remote computer with preset execution date and time, to install updates
    After execution writes log to c:\temp\windows-updates-install-log.txt

    Script is written using Windows Update Agent API
    https://msdn.microsoft.com/en-us/library/windows/desktop/aa387099(v=vs.85).aspx

.EXAMPLE
    .\windows-updater-using-WUA-API-v2.ps1 -computer servername -excludelist 3140250, 3140234 -includeoptionalupdates
.NOTES
   	Version 2.0
   	Date: 07.04.2016

   	Use at own risk!
#>

 param (
    [switch]$includeoptionalupdates = $false,
    [switch]$rebootifrequired,
    [array]$excludelist,
    [string]$computer = $("-computer parameter is required.")
 )

#because of a bug can not pass [switch] parameter the way it is to Register-ScheduledJob cmdlet, have to convert it to [string]
if ($rebootifrequired) {
    [string]$rebootifrequired = "true"
    }

#create new remote session
$remotesession = New-PSSession -ComputerName $computer

#full search criteria list available https://msdn.microsoft.com/en-us/library/windows/desktop/aa386526(v=vs.85).aspx

#check if to include or not optional updates into search
if ($includeoptionalupdates) {
    #define search criteria with optional updates
    $searchcriteria = "isinstalled=0 and type='Software'"
    }
else {
    #define search criteria without optional updates
    $searchcriteria = "isinstalled=0 and type='Software' and BrowseOnly=0" #IsAssigned=1"
    }

#enter remote session
Invoke-Command -Session $remotesession -ArgumentList $searchcriteria, $excludelist, $rebootifrequired -scriptblock {
    $searchcriteria = $args[0]
    $excludelist = $args[1]
    $rebootifrequired = $args[2]

    #search for available updates according to search criteria
    $comobject = New-Object -ComObject Microsoft.Update.Session
    $searcherresult = $comobject.CreateupdateSearcher().Search($searchcriteria).Updates

    #list search results
    ""
    "Available updates:"
    foreach ($searchitem in $searcherresult) {
        "KB$($searchitem.KBArticleIDs): $($searchitem.title)"
        }
    "Total: $($searcherresult.count)"
    ""

    #if exclude list is given
    if ($excludelist) {

        #list excluding updates
        "Excluding updates:"
        $actuallyexcluding = @()
        $realexcludelist = @()
        foreach ($excludeitem in $excludelist) {
            $checkcode = 0
            foreach ($searchitem in $searcherresult) {
                if ([string]$excludeitem -eq [string]$searchitem.KBArticleIDs) {
                    $actuallyexcluding += "KB$($excludeitem): $($searchitem.title)"
                    $realexcludelist += $excludeitem
                    "KB$($excludeitem): $($searchitem.title)"
                    $checkcode = 1
                    }
                }
            if ($checkcode -ne 1) {
                write-host -foregroundcolor "magenta" "KB$($excludeitem): ERROR! update not listed in available updates list"
                }
            }
        "Total: $($actuallyexcluding.Count)"
        ""

        #form new search criteria which only includes updates we want to install
        $searchcriteria = @()
        $checkcodeA = 0
        for ($i=0; $i -lt $Searcherresult.count; $i++) {
            $checkcodeB = 0
            foreach ($excludesingleitem in $realexcludelist) {

                if ($Searcherresult.Item($i).KBArticleIDs -eq $excludesingleitem) {
                    $checkcodeB = 1
                    }
                }

                if ($checkcodeB -eq 0) {
                    if (!($checkcodeA -eq 0)) {
                        $searchcriteria += "or UpdateID='$($Searcherresult.Item($i).Identity.updateid)'"
                        }
                    else {
                        $searchcriteria += "UpdateID='$($Searcherresult.Item($i).Identity.updateid)'"
                        $checkcodeA = 1
                        }
                    }
            }

        #form new search criteria for updates into a string
        [string]$searchcriteria = "$([system.String]::Join(" ", $searchcriteria))"

        }

    #check if searchcriteria is not empty
    if ([string]::IsNullOrEmpty($searchcriteria)) {
        $searchcriteriastatus = "empty"
        "Download and Install updates:"
        "Total: 0"
        }
    else {
        #search for updates according to new criteria
        $searcherresult = $comobject.CreateupdateSearcher().Search($searchcriteria).Updates

        #list all updates that would be downloaded and installed
        "Download and Install updates:"
        foreach ($searchitem in $searcherresult) {
            "KB$($searchitem.KBArticleIDs): $($searchitem.title)"
            }
        "Total: $($searcherresult.count)"
        }
    }


#check if Download and Install updates are empty, exit if so
$checkifempty = (Invoke-Command -Session $remotesession -scriptblock {

    #check if searchcriteria is empty or if at least one available update exist
    if (($searchcriteriastatus -eq "empty") -or ($searcherresult.count -eq "0")) {
        "empty"
        }
    })

if ($checkifempty -eq "empty") {
    ""
    "There is nothing to update, exiting.."
    exit
    }


#read user input
""
[string]$downloadandinstall = $(Read-Host "Proceed with setting up Download and Install? Y/N")

if ($downloadandinstall -eq "y") {

    #set time updates should run
    $errorcode =1
    while ($errorcode -eq 1) {
        ""
        [string]$jobstarttime = $(Read-Host "Set date and time updates should be installed at (input any format of date ant time, like $((get-date).ToString())) or type Quit")
        if ($jobstarttime -eq "quit") {
            ""
            "exiting install."
            exit
            }

        try {
            if ((Get-Date -Date $jobstarttime)) {
                "" 
                "You preset : $(Get-Date -Date $jobstarttime)"
                ""
                [string]$proceed = $(Read-Host "Is this date and time correct? Proceed placing sheduled job for Download and Install updates? Y/N/Quit")
                if ($proceed -eq "y") {
                    $errorcode = 0
                    }
                elseif ($proceed -eq "quit") {
                    ""
                    "exiting install."
                    exit
                    }
                else {
                    $errorcode = 1
                    }
                }
            }
        catch {
            ""
            write-host -foregroundcolor "red" "Wrong date time format, try again"
            $errorcode = 1
            }
        }


    #return to remote session again
    Invoke-Command -Session $remotesession -ArgumentList $jobstarttime -scriptblock {
        $jobstarttime = $args[0]

        try {
            #if temp path not exist create it (for log file)
            if (-not (Test-Path "c:\temp")) {
                New-Item "c:\temp" -ItemType directory | Out-Null
                }
            }
        catch {
            "Error occured while trying to create c:\temp folder:"
            ""
            $Error[0].Exception
            }

        #Get Date for a job name
        $DateAndTime = (Get-Date -format ddMMMyyyy-HH.mm)

        try {
            #Register Scheduled Task with Date
            $triger = New-JobTrigger -Once -At $jobstarttime
            $job = Register-ScheduledJob -Name "InstallUpdates $DateAndTime" -ArgumentList $searchcriteria, $rebootifrequired -Trigger $triger -ScriptBlock {
                $searchcriteria = $args[0]
                $rebootifrequired = $args[1]
                $resultfile = "c:\temp\windows-updates-install-log.txt"                
                $resultarray = @()

                #check if users logged in
                $loggedinusers = @(query user)

                if ($loggedinusers.length -gt 1) {
                    $resultarray += @(Get-Date)
                    $resultarray += @("")
                    $resultarray += @($loggedinusers)
                    $resultarray += @("")
                    $resultarray += @("")
                    $resultarray += @("Skipping updating because at least one user is logged in!")
                    $resultarray | Out-File $resultfile
                    exit
                    }

                #search for available updates according to previous defined search criteria
                $comobject = New-Object -ComObject Microsoft.Update.Session
                $searcherresult = $comobject.CreateupdateSearcher().Search($searchcriteria).Updates

                #list all updates that would be downloaded and installed
                $updatelist = foreach ($searchitem in $searcherresult) {
                    "KB$($searchitem.KBArticleIDs): $($searchitem.title)"
                    }
                $resultarray += @(Get-Date)
                $resultarray += ""
                $resultarray += @("Download and Install updates:")
                $resultarray += @($updatelist)
                $resultarray += @("Total: $($searcherresult.count)")
                $resultarray += @("")
                $resultarray += @("Result:")

                #download updates    
                $download = $comobject.CreateUpdateDownloader() 
                $download.Updates = $searcherresult 
                $downloadresult = $download.download()

                $resultarray += @(switch ($downloadresult.resultcode) 
                    { 
                    2 {"Download Succeeded"} 
                    3 {"Download Succeeded with Errors"} 
                    4 {"Download Failed"} 
                    5 {"Download Aborted"} 
                    default {"Download result code is not defined $($downloadresult.resultcode)"}
                    })

                #install updates
                $install = $comobject.CreateUpdateInstaller()
                $install.updates = $searcherresult
                $installresult = $install.install()
        
                $resultarray += @(switch ($installresult.resultcode) 
                    { 
                    2 {"Install Succeeded"} 
                    3 {"Install Succeeded with Errors"} 
                    4 {"Install Failed"} 
                    5 {"Install Aborted"} 
                    default {"Install result code is not defined $($installresult.resultcode)"}
                    })
                
                #check if switch "-rebootifrequired" was used
                if ($rebootifrequired) {

                    #Reboot if required by updates.
                    If ($installresult.rebootRequired) {
                        $resultarray += @("")
                        $resultarray += @("Restart computer command executed!")
                        $restart = "yes"
                        }
                    }
                else {
                    #Inform if reboot required by updates.
                    If ($installresult.rebootRequired) {
                        $resultarray += @("")
                        $resultarray += @("To finish updating reboot required!")
                        }
                    }
                                                  
                #write logfile
                $resultarray | Out-File $resultfile

                #restart computer
                if ($restart -eq "yes") {
                    try {
                        #restarts computer if no users logged in. if they are - return error
                        Restart-Computer -ErrorAction Stop
                        }
                    catch {
                        $resultarray = @()
                        $resultarray += @($Error[0].Exception)
                        $resultarray += @(query user)
                        $resultarray += @("To finish updating reboot required!")
                        $resultarray | Out-File -Append $resultfile
                        }
                    }
                }
            }

        catch {
            write-host -foregroundcolor "red" "Error occured while trying to place a sheduled job:"
            ""
            write-host -foregroundcolor "red" $Error[0].Exception
            }

        #create new psobject with properties
        $jobobject = New-Object -TypeName psobject | Select-Object JobName, Runtime, Enabled
        $jobobject.jobname = $job.name
        $jobobject.runtime = ($job.JobTriggers).at
        $jobobject.enabled = $job.Enabled
        $jobobject
        }
    ""
    "You can check how it went on \\$($computer)\c$\temp\windows-updates-install-log.txt"
    }

else {
    ""
    "exiting install."
    exit
    }

