﻿<#
.NAME
    Change default gateway
.DESCRIPTION
    This script changes default gateway for the multiple computers. It fetches computer objects from the specified AD OU and iterates through them changing default gateway. This sript uses
    Vexasoft Cmdlet Library available from here http://www.vexasoft.com/ , it is paid library, but has 30 day trial period which is enough for one time bulck change. 
#>

$newGateway = "10.1.255.254" 
$OUPath = 'OU=PS,OU=Servers,DC=visma,DC=asp'
$ADComputers = Get-ADComputer -SearchBase $OUPath -Filter {(Name -Like "*") -And (Enabled -eq "True")} | Sort-Object -Property Name | Select-Object -expand Name #| select -first 21
$listOfComputersWithChangedGateway = New-Object System.Collections.ArrayList


function Get-ServersWithDefaultGateway($servers){
    <#
  .SYNOPSIS
  Displays default gateway for all servers in the passed array.
  .PARAMETER computername
  List of computers to query.
  #>

    foreach ($server in $servers){
    $serverAdapter = Get-NetworkAdapter -ComputerName $server
    Write-Host $server, $serverAdapter.name, $serverAdapter.gateway
}
}

function Set-DefaultGateway($servers){
    <#
  .SYNOPSIS
  CHanges default gateway for all servers in the passed array.
  .PARAMETER computername
  List of computers to change gateway.
  #>

    foreach ($server in $servers){
    $serverAdapter = Get-NetworkAdapter -ComputerName $server
    $serverAdapterName = $serverAdapter.name
    $serverAdapterGateway =  $serverAdapter.gateway
    
    #Write-Host $server, $serverAdapterName, $serverAdapterGateway
    
    if ($serverAdapterGateway -ne "10.1.255.254"){
      #  Write-Host "Set-NetworkAdapterGateway -ComputerName $server -Name $serverAdapterName -Gateway 10.1.255.254"
        #Set-NetworkAdapterGateway -ComputerName "$server" -Name "$serverAdapterName" -Gateway "$newGateway"
        $listOfComputersWithChangedGateway.Add($server)
    }
    
}
}


Get-ServersWithDefaultGateway($ADComputers)

Set-DefaultGateway($ADComputers)
Write-Host "Network gateway was changed for the following servers: $listOfComputersWithChangedGateway"

Get-ServersWithDefaultGateway($ADComputers)